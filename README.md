# switch-desk

Based on the work of dj1s from his tutorial at [Linux Mint Forums](https://forums.linuxmint.com/viewtopic.php?f=42&t=360221)

As of writing (04-2024) there's no way in Linux Mint Cinnamon Edition to have dedicated Backgrounds (color/wallpaper) per Workspace (aka virtual desktops)

To circumvent this, DJ1S proposed a set of bash scripts, that leverage this. This basic work is enhanced to accomplish easier deployment and setup, aswell as taking care of international differences in the naming of the system folder "Desktop", where the most magic happens

## Limitations
This solution relies on using certain keyboard shortcuts to switch desktops. If you use others like "hot corner", "desktop overview" , "alt-tab switching" and so on, the backgrounds are _not_ changed.

## Getting started

All starts with a 

``git clone https://gitlab.com/thetagamma11/switch-desk.git``

## Installation
execute
```console
cd switch-desk; bash init-switch-desk.sh
```


## Usage
After you log out and in again, you can use the keyboard shortcuts <CTRL><Alt>Left and <CTRL><Alt>Right to switch the desktops. Now change your background color and/or the background color with your standard Mint methods (e.g. right click on Desktop -> change background )

## Support
Just open an issue if you find a bug, 

## Roadmap
If you have ideas for releases in the future, please file an issue

## Contributing
Just open a merge request to the development branch 

## Authors and acknowledgment
* Thetagamma
* Acknowledgement: DJ1S

## License
MIT License

## Project status
Loosely updated as time and feedback comes in.