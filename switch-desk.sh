#!/bin/bash
# work based on DJ1S switch desk tutorial on Linux Mint Forums https://forums.linuxmint.com/viewtopic.php?f=42&t=360221
#
# Purpose: Switch Workspace to $1 Workspace with its own Background, Color and Desktop icons
#
#

cd ~
source ~/.config/user-dirs.dirs # Name of "Desktop" changes for different languages e.g. "Bureau" in FR or "Schreibtisch" in DE
#echo "XDG_DESKTOP_DIR $XDG_DESKTOP_DIR"
BGpic=$(gsettings get org.cinnamon.desktop.background picture-uri)   # Get the current background path
BGcol=$(gsettings get org.cinnamon.desktop.background primary-color) # Get the current background color
#echo "BG = $BGpic"
# use dotfiles so that our configuration items do not appear on Desktop
ln -sfn $BGpic $XDG_DESKTOP_DIR/.background   #Save the path as a soft link in background on the current Desktop
echo $BGcol >  $XDG_DESKTOP_DIR/.primary-color #Save the current BG color
ln -sfn $XDG_DESKTOP_DIR$1 $XDG_DESKTOP_DIR    #Make Desk$1 the new Desktop
wmctrl -s $(($1-1))  #Use wmctrl to switch Workspace (N.B., 1st workspace is 0)
BGnew=$(readlink $XDG_DESKTOP_DIR$1/.background)  #Fetch the new background from the background soft link in Desk$1
BGcolnew=$(cat $XDG_DESKTOP_DIR/.primary-color) #Fetch the new background color from file ##TODO Either initialize files during setup or do some error handling if file doesn't exist
#echo "BG = $BGnew"
gsettings set org.cinnamon.desktop.background picture-uri $BGnew  #Change the background in Desk$1
gsettings set org.cinnamon.desktop.background primary-color $BGcolnew #Change BGcolor
gsettings set org.nemo.desktop desktop-layout 'false::false'
gsettings set org.nemo.desktop desktop-layout 'true::false'
#end of script