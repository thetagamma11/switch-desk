#!/bin/bash
# initialize switch-desk
NUMBER_OF_WORKSPACES=$(gsettings get org.cinnamon.desktop.wm.preferences num-workspaces)

# Check if NUMBER_OF_WORKSPACES is less than or equal to 1
if [ "$NUMBER_OF_WORKSPACES" -le 1 ]; then
    echo "Sorry, it seems no addidtional workspaces are configured - Stopping here"
    exit 0  # Exit gracefully
fi

source ~/.config/user-dirs.dirs || { echo "Error: could not find localization configuration"; exit 1; } # Name of "Desktop" changes for different languages e.g. "Bureau" in FR or "Schreibtisch" in DE

# Check if XDG_DESKTOP_DIR is empty
if [ -z "$XDG_DESKTOP_DIR" ]; then
    echo "Error: XDG_DESKTOP_DIR from user-dirs is empty. Please set it to a valid directory."
    exit 1
fi

# create Directories
for ((i=1;i<=NUMBER_OF_WORKSPACES;i++)); do
    echo "check/create ${XDG_DESKTOP_DIR}$i"
    # in case we add workspaces, the goal should be to run this script again and leave existing configuration untouched
    if [ ! -d "${XDG_DESKTOP_DIR}$i" ]; then
        mkdir ${XDG_DESKTOP_DIR}$i || { echo "Error: Unable to create directory ${XDG_DESKTOP_DIR}$i"; exit 1; }
        cp -a ${XDG_DESKTOP_DIR}/* ${XDG_DESKTOP_DIR}$i # make initially appear all icons on all workspaces
        echo "added Folder for Workspace $i"
    else
        echo "Folder ${XDG_DESKTOP_DIR}$i already exists - nothing changed"
    fi
done


# backup Desktop Folder
if [ ! -h "${XDG_DESKTOP_DIR}"] || [ ! -e ${XDG_DESKTOP_DIR}.org]; then
    mv ${XDG_DESKTOP_DIR} ${XDG_DESKTOP_DIR}.org
    echo "File moved successfully."
else
    echo "Skipping: Backup exists or existing configuration found"
fi

# create initally a link to first workspace folder
ln -sfn ${XDG_DESKTOP_DIR}1 ${XDG_DESKTOP_DIR}

target_dir="$HOME/bin"

# Check if $HOME/bin exists
if [ ! -d "$target_dir" ]; then
    echo "Directory $target_dir does not exist. Creating directory..."
    mkdir -p "$target_dir" || { echo "Error: Unable to create directory $target_dir"; exit 1; }
fi

# Check if $target_dir is in PATH
if [[ ":$PATH:" != *":$target_dir:"* ]]; then
    # Add $target_dir to PATH in shell environment
    echo "Adding $target_dir to PATH..."
    echo "export PATH=\"\$PATH:$target_dir\"" >> "$HOME/.bashrc"
    export PATH="$PATH:$target_dir" || { echo "Error: Unable to add $target_dir to PATH"; exit 1; }
    echo "$target_dir added to PATH."
fi

# Copy files to $HOME/bin and make them executable
echo "Copying files to $target_dir..."
cp -r *.sh "$target_dir" || { echo "Error: Unable to copy files to $target_dir"; exit 1; }
chmod +x $target_dir/*.sh
echo "Files copied successfully to $target_dir."

echo "All set up! now we onfigure your keyboard hooks..."

if [[ ! "$XDG_CURRENT_DESKTOP" =~ "Cinnamon" ]]; then
    echo "Sorry Script currently only works with Cinnamon, not with $XDG_CURRENT_DESKTOP. Please configure your keyboard hooks manually"
    exit 0


# first read currently configured custom keyboard shortcuts
KEYBINDINGS=$(dconf read /org/cinnamon/desktop/keybindings/custom-list)

# check if we have already the keybindings configured
EVIDENCE="switch-"
if [[ "$KEYBINDINGS" =~ $EVIDENCE ]]; then
    echo "It seems you already have the keyboard shortcuts"
else
    echo "No previous workspace switch shortcuts found - let's go..."
    # add the new bindings to the overall list of bindings
    KEYBINDINGS="${KEYBINDINGS:0:-1}, 'switch-right', 'switch-left']"
    dconf write /org/cinnamon/desktop/keybindings/custom-list "$KEYBINDINGS"
    # now write the keyboard shortcuts
    dconf write /org/cinnamon/desktop/keybindings/custom-keybindings/switch-left/binding "['<Primary><Alt>Left']"
    dconf write /org/cinnamon/desktop/keybindings/custom-keybindings/switch-left/command '"switch-desk-left.sh"'
    dconf write /org/cinnamon/desktop/keybindings/custom-keybindings/switch-left/name '"switch-workspace-left"'
    dconf write /org/cinnamon/desktop/keybindings/custom-keybindings/switch-right/binding "['<Primary><Alt>Right']"
    dconf write /org/cinnamon/desktop/keybindings/custom-keybindings/switch-right/command '"switch-desk-right.sh"'
    dconf write /org/cinnamon/desktop/keybindings/custom-keybindings/switch-right/name '"switch-workspace-right"'
fi

#
echo "Setup finished. To make your new PATH work, you should log out and in again. Have fun!"
# 

